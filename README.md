# Code Quality-based MR approvals

## Setup

To run code quality scanning with approvals, simply add the following to your `.gitlab-ci.yml`:

```yaml
include:
  - remote: 'https://gitlab.com/smathur/code-quality-mr-approvals/raw/master/code_quality_approvals.gitlab-ci.yml'
```

Configure your code quality scanning with MR approvals, using the following variables:

| Variable              | Description                                                                                                                             | Example           | Required               |
|-----------------------|-----------------------------------------------------------------------------------------------------------------------------------------|-------------------|------------------------|
| CQ_APPROVALS_COUNT    | Number of desired approvals if code quality issues are found                                                                            | 1                 | Yes                    |
| CQ_APPROVERS          | List of GitLab usernames that can approve a MR when code quality issues are found                                                       | "gitlab_username" | Yes                    |
| CQ_MR_ACCESS_TOKEN    | Token with **api** scope and the **Developer** role or higher. This token should be saved as a CI/CD variable (e.g. `MY_ACCESS_TOKEN`), which we reference here. When setting the CI/CD variable, make sure to **uncheck** `Protect variable` so that feature branches can access it. | $MY_ACCESS_TOKEN  | Yes                    |
| CQ_MR_THRESHOLD       | **Minimum** number of code quality issues to look for before requiring MR approval                                                      | 4                 | No (defaults to 1)     |
| CQ_MR_COMPARE_DEFAULT | If set to "true", the approval rule threshold only considers new code quality issues that are not present in the default branch. If set to "false", all code quality issues are counted. | "true"            | No (defaults to "false") |

As an example, you may configure a `variables` section **after** the `include` section as follows:
```yaml
variables:
  CQ_APPROVALS_COUNT: 1
  CQ_APPROVERS: "gitlab_username"
  CQ_MR_ACCESS_TOKEN: $MY_ACCESS_TOKEN
```

## Example

You can see an example of this in action in [this merge request](https://gitlab.com/smathur/cq-mr-approval-test/-/merge_requests/3). An approval rule `Code Quality check` has been created due to code quality issues being found.