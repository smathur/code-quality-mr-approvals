#!/bin/bash

RULE_NAME="Code Quality check"

# check if $CQ_MR_COMPARE_DEFAULT = "true"
if [ "$CQ_MR_COMPARE_DEFAULT" = "true" ]; then

    curl --location --header "PRIVATE-TOKEN: $CQ_MR_ACCESS_TOKEN" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/jobs/artifacts/${CI_DEFAULT_BRANCH}/raw/gl-code-quality-report.json?job=code_quality" > default-branch-gl-code-quality-report.json
    echo "${CI_DEFAULT_BRANCH} branch code quality issues:"
    jq . default-branch-gl-code-quality-report.json
    echo "${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME} branch code quality issues:"
    jq . gl-code-quality-report.json

    # parse default-branch-gl-code-quality-report.json, and compare to gl-code-quality-report.json, which already exists in pwd
    # defects = number of list items in gl-code-quality-report.json that are not present in default-branch-gl-code-quality-report.json. To determine if a list item is present from one file vs another, the "fingerprint" value in each list item can be used.
    # Parse the default-branch-gl-code-quality-report.json file
    default_branch_report=$(cat default-branch-gl-code-quality-report.json)
    default_branch_fingerprints=($(echo "$default_branch_report" | jq -r '.[] | .fingerprint'))
    # Parse the gl-code-quality-report.json file
    report=$(cat gl-code-quality-report.json)
    fingerprints=($(echo "$report" | jq -r '.[] | .fingerprint'))
    # Calculate the number of new defects
    defects=0
    for fingerprint in "${fingerprints[@]}"; do
        if [[ ! "${default_branch_fingerprints[@]}" =~ "$fingerprint" ]]; then
            defects=$((defects + 1))
        fi
    done
else
    defects=$(jq length gl-code-quality-report.json)
fi

echo "Number of defects: $defects"
echo "Code quality threshold: ${CQ_MR_THRESHOLD}"

if ((defects >= CQ_MR_THRESHOLD))
then
    echo "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/approval_rules"
    echo "Access token: ${CQ_MR_ACCESS_TOKEN}"
    echo "Approvals required: ${CQ_APPROVALS_COUNT}"
    echo "Approvers: ${CQ_APPROVERS}"
    curl -X POST -H "Content-Type: application/x-www-form-urlencoded" -H "PRIVATE-TOKEN: $CQ_MR_ACCESS_TOKEN" -d "approvals_required=$CQ_APPROVALS_COUNT&name='$RULE_NAME'&usernames=$CQ_APPROVERS" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/approval_rules"
fi
